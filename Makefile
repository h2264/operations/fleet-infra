.PHONY: cluster-install cluster-clean
CLUSTER=multi-node
cluster-install:
	kind create cluster --config=$(CLUSTER).yaml --name=$(CLUSTER)
cluster-clean:
	kind delete cluster --name=$(CLUSTER)

.PHONY: argocd-install argocd-proxy argocd-password argocd-login argocd-clean argocd-wait
NAMESPACE=argocd
ARGOCD_PASSWORD=$$(kubectl -n $(NAMESPACE) get secret argocd-initial-admin-secret -o yaml | grep -o 'password: .*' | sed -e s"/password\: //g" | base64 -d)
argocd-install:
	kubectl create ns $(NAMESPACE)
	kubectl -n $(NAMESPACE) apply  -f https://raw.githubusercontent.com/argoproj/argo-cd/master/manifests/ha/install.yaml
	$(MAKE) argocd-wait

argocd-wait:
	kubectl -n $(NAMESPACE) wait -l app.kubernetes.io/name=argocd-server --for=condition=ready pod --timeout=360s

argocd-proxy:
	kubectl -n $(NAMESPACE) port-forward svc/argocd-server 8080:443

argocd-password:
	@echo "ARGOCD_PASSWORD: $(ARGOCD_PASSWORD)"

argocd-login: argocd-password
	@argocd login localhost:8080 --insecure --username="admin" --password="$(ARGO_PASSWORD)"

argocd-bootstrapp:
	argocd app create fleet-infra \
		--dest-namespace $(NAMESPACE) \
		--dest-server https://kubernetes.default.svc \
		--repo https://gitlab.com/h2264/operations/fleet-infra.git \
		--path apps  
	argocd app sync fleet-infra

.PHONY: bootstrapp
bootstrapp: bootstrapp-cluster bootstrapp-argocd

# Internal Services
# LONGHORN
.PHONY: longhorn-proxy
longhorn-proxy:
	kubectl -n longhorn-system port-forward svc/longhorn-frontend 8081:80

# ARGO WORKFLOWS
.PHONY: workflow-token
ARGO_TOKEN="Bearer $$(kubectl -n argo-workflows get secret server.service-account-token -o=jsonpath='{.data.token}' | base64 --decode)"
workflow-token:
	@echo $(ARGO_TOKEN)

# MONITORING STACK
.PHONY: grafana-password grafana-proxy
GRAFANA_PASSWORD=$$(kubectl -n monitoring-stack get secret monitoring-stack-grafana -o=jsonpath='{.data.admin-password}' | base64 -d)
grafana-password:
	@echo $(GRAFANA_PASSWORD)
grafana-proxy:
	kubectl -n monitoring-stack port-forward svc/monitoring-stack-grafana 8080:80
