
# Bootstrap

## Assumptions

- Operational k3s cluster
- Administrative access to k3s cluster

## Getting Started

```bash
#!/bin/bash
# Install kind - Kubernetes in Docker. See https://kind.sigs.k8s.io/docs/user/quick-start/

# Create kind cluster
kind create cluster --config=multi-node.yaml

# Determine cluster node(s) status
kubectl get nodes

# Install HA argocd
export NAMESPACE=argocd
kubectl create ns $NAMESPACE
kubectl -n $NAMESPACE apply  -f https://raw.githubusercontent.com/argoproj/argo-cd/master/manifests/ha/install.yaml

# Monitor argocd deployment(s)
watch kubectl -n $NAMESPACE get pods

# Get credentials
kubectl -n argocd get secret argocd-initial-admin-secret -o yaml | grep -o 'password: .*' | sed -e s"/password\: //g" | base64 -d

# Proxy the argocd-server
kubectl -n $NAMESPACE port-forward svc/argocd-server -n argocd 8080:443

# Login to argocd
argocd login localhost:8080

# Self manage the argocd app
argocd app create fleet-infra \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/h2264/operations/fleet-infra.git \
    --path apps  
argocd app sync fleet-infra
```
