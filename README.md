# Fleet Infrastructure

This repository is intended for cluster bootstrapping with the end goal of providing a platform with the following traits.

- [GitOps driven](https://opengitops.dev/about).
- [ArgoCD app of apps pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern).
- [Kubernetes native](https://landscape.cncf.io/).

## Fleet Infra Applications

Applications are installed through helm and kustomize. Applications in scope are the following:

| Name | Installation Type | Status | Comment |
| --- | --- | --- | --- |
| argocd | kustomize | ![argocd](https://cd.cloudycloud.live/api/badge?name=argocd&revision=true&showAppName=true) | Infrastructure management application |
| argo-workflows | helm | ![argo-workflows](https://cd.cloudycloud.live/api/badge?name=argo-workflows&revision=true&showAppName=true) | Workflow templates application |
| cert-manager | helm | ![cert-manager](https://cd.cloudycloud.live/api/badge?name=cert-manager&revision=true&showAppName=true) | Certificates management |
| external-secrets | helm | ![external-secrets](https://cd.cloudycloud.live/api/badge?name=external-secrets&revision=true&showAppName=true) | K8s secrets integration tool |
| gitlab-runner | helm | ![gitlab-runner](https://cd.cloudycloud.live/api/badge?name=gitlab-runner&revision=true&showAppName=true) | GitLab Runner primarily used for CI |
| harbor | helm | ![harbor](https://cd.cloudycloud.live/api/badge?name=harbor&revision=true&showAppName=true) | Helm and OCI repository |
| k3s-upgrader | kustomize | ![k3s-upgrader]( https://cd.cloudycloud.live/api/badge?name=k3s-upgrader&revision=true&showAppName=true) | k3s upgrader application |
| longhorn | helm | ![longhorn](https://cd.cloudycloud.live/api/badge?name=longhorn&revision=true&showAppName=true) | Storage Management Tool |
| monitoring-stack | helm | ![monitoring-stack](https://cd.cloudycloud.live/api/badge?name=monitoring-stack&revision=true&showAppName=true) | Infrastructure monitoring stack |
| nfs-provisioner | helm | ![nfs-provisioner](https://cd.cloudycloud.live/api/badge?name=nfs-provisioner&revision=true&showAppName=true) | NFS Storage Class |
| nvidia-operator | helm | ![nvidia-operator](https://cd.cloudycloud.live/api/badge?name=nvidia-operator&revision=true&showAppName=true) | GPU (NVIDIA) Operator |
| vault | helm | ![vault](https://cd.cloudycloud.live/api/badge?name=vault&revision=true&showAppName=true) | Secrets Management Tool |
|  |  |  |  |

## App of apps

| Name | Installation Type | Status | Comment |
| --- | --- | --- | --- |
| [fleet-infra](https://gitlab.com/h2264/operations/fleet-infra.git) | helm | ![fleet-infra](https://cd.cloudycloud.live/api/badge?name=fleet-infra&revision=true&showAppName=true) | Infrastructure Application installation |
| [fleet-ops](https://gitlab.com/h2264/operations/fleet-ops.git) | helm | ![fleet-ops](https://cd.cloudycloud.live/api/badge?name=fleet-ops&revision=true&showAppName=true) | Infrastructure Operations runtime automation |
| [soc-med](https://gitlab.com/h2264/operations/soc-med.git) | helm | ![soc-med](https://cd.cloudycloud.live/api/badge?name=soc-med&revision=true&showAppName=true) | Social Media related applications |
|  |  |  |  |
